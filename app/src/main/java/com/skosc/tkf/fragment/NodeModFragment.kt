package com.skosc.tkf.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.skosc.tkf.R
import com.skosc.tkf.TKFApplication
import com.skosc.tkf.adapter.NodeListPagerAdapter
import com.skosc.tkf.viewmodel.NodeModViewModel
import com.skosc.tkf.viewmodel.NodeModViewModelFactory
import kotlinx.android.synthetic.main.fragment_node_mod.*
import org.kodein.di.direct
import org.kodein.di.generic.instance

class NodeModFragment : Fragment() {
    private val rootId by lazy {
        arguments!!.getLong("rootId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_node_mod, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        viewpager.adapter = NodeListPagerAdapter(rootId, childFragmentManager)
    }
}
