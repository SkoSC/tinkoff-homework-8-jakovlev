package com.skosc.tkf

import android.app.Application
import androidx.room.Room
import com.skosc.tkf.room.NodeDao
import com.skosc.tkf.room.Repository
import com.skosc.tkf.room.TKFDatabase
import com.skosc.tkf.viewmodel.AllNodsViewModelFactory
import com.skosc.tkf.viewmodel.NodeModViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.generic.*

class TKFApplication : Application() {
    val kodein = Kodein.lazy {
        val db = Room.databaseBuilder(applicationContext, TKFDatabase::class.java, "tkf-nodes")
            .fallbackToDestructiveMigration()
            .build()

        bind<NodeDao>() with singleton { db.nodeDao }
        bind<Repository>() with provider { Repository(instance()) }
        bind<NodeModViewModelFactory>() with factory { id: Long -> NodeModViewModelFactory(kodein, id) }
        bind<AllNodsViewModelFactory>() with provider { AllNodsViewModelFactory(kodein) }
    }

    override fun onCreate() {
        super.onCreate()
    }
}
