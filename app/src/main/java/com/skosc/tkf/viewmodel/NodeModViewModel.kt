package com.skosc.tkf.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.skosc.tkf.room.Node
import com.skosc.tkf.room.Repository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class NodeModViewModel(rootId: Long, val repo: Repository) : ViewModel() {
    private val cdisp = CompositeDisposable()
    val currentNode = MutableLiveData<Node>()

    val children = MutableLiveData<List<Node>>()
    val parents = MutableLiveData<List<Node>>()

    init {
        children.value = listOf()
        parents.value = listOf()

        cdisp.add(repo.find(rootId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                currentNode.value = it
            })

        currentNode.observeForever { node ->
            cdisp.add(Observable.interval(1, TimeUnit.SECONDS).flatMap { repo.children(node) }
                .flatMap { children -> repo.notChildren(node).map { children + it } }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    children.value = it
                })

            cdisp.add(Observable.interval(1, TimeUnit.SECONDS).flatMap { repo.parents(node) }
                .flatMap { parents -> repo.notChildren(node).map { parents + it } }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    parents.value = it
                })
        }
    }

    fun addParents(parents: List<Node>) {
        repo.addParents(currentNode.value!!, parents)
    }

    fun addChildren(children: List<Node>) {
        repo.addChildren(currentNode.value!!, children)
    }

    fun deleteRelationWith(child: Node) {
        val current = currentNode.value!!
        repo.removeRelation(current, child)
    }

    override fun onCleared() {
        super.onCleared()
        cdisp.dispose()
    }
}