package com.skosc.tkf.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "node_relation",
    foreignKeys = []
)

data class NodeRelation(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0,

    @ColumnInfo(name = "parent")
    val parent: Long,

    @ColumnInfo(name = "child")
    val child: Long
)